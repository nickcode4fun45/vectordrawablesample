package net.nickcode4fun.vectordrawablesimple

import android.graphics.drawable.Animatable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun animStart(v: View) {
        if (v is ImageView) {
            val drawable = v.drawable
            if (drawable is Animatable) {
                drawable.start()
            }
        }
    }
}
